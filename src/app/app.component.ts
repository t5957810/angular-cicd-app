import { Component, OnInit } from '@angular/core';
import packageJson from 'package.json';
import { BaseServiceService } from './base-service.service';
import { EMPTY, catchError, filter, forkJoin, of, retry, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angular-cicd-app';
  version = 'v' + packageJson.version;

  communityDetail: any;
  communityList: any;
  market591List: any;
  market591Detail: any;

  isComplete = false;
  isLoading = false;
  loadingText = 'Loading...';

  constructor(
    private baseServiceService: BaseServiceService
  ) { }

  ngOnInit(): void {
    this.query();
  }

  private update() {
    this.communityList = this.loadingText;
    this.communityDetail = this.loadingText;
    this.market591List = this.loadingText;
    this.market591Detail = this.loadingText;
  }

  public query(): void {
    this.isLoading = true;
    this.loadingText = 'Loading...';
    this.isComplete = false;

    this.update();
    const queryCommunityList$ = this.baseServiceService.queryCommunityList();
    const queryCommunityDetail$ = this.baseServiceService.queryCommunityDetail();
    const queryMarket591List$ = this.baseServiceService.queryMarket591List();
    const queryMarket591Detail$ = this.baseServiceService.queryMarket591Detail();

    forkJoin([queryCommunityList$, queryCommunityDetail$, queryMarket591List$, queryMarket591Detail$]).pipe(
      filter(() => !this.isComplete),
      // retry({ count: 3, delay: 1000 }),
      catchError((error) => {
        return of(error); // 返回一個錯誤的Observable以保持forkJoin的完成
      })
    ).subscribe((result: any[]) => {
      console.log('result=', result);
      if (result instanceof HttpErrorResponse) {
        console.log('發生錯誤!')
        this.isLoading = false; 
        this.loadingText = 'Error! 請重新整理!';
        this.update();
        return;
      } 

      this.communityList = result[0];
      this.communityDetail = result[1];
      this.market591List = result[2];
      this.market591Detail = result[3];
      this.isComplete = true;
      this.isLoading = false;
      
    });


  }
}
