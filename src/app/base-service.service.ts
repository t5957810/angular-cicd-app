import { Injectable } from '@angular/core';
import { HttpBackend, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseServiceService {
  private COMMUNITY_HOUSEPRICE_URL = 'https://community.houseprice.tw/ws/building/';
  private COMMUNITY_HOUSEPRICE_DETAIL_URL = 'https://price.houseprice.tw/ws/dealcase/';
  private MARKET_591_HOUSEPRICE_URL = 'https://bff.591.com.tw/v1/community/price/';
  private MARKET_591_HOUSEPRICE_DETAIL_URL = 'https://bff.591.com.tw/v1/community/price/detail?id=';


  constructor(
    private httpclient: HttpClient
  ) { }

  private encodeURIComponent(url: string, isNeedEncode = true) {
    if (isNeedEncode) {
      return encodeURIComponent(url);
    }

    return url;
  }

  public queryCommunityList(buildId: string = '35052'): Observable<any> {
    return this.httpclient.get(environment.corsUrl + this.encodeURIComponent(this.COMMUNITY_HOUSEPRICE_URL + buildId + '/price/date-desc_sort/?p=1'));
  }

  public queryCommunityDetail(buildDetailId: string = '8576947'): Observable<any> {
    return this.httpclient.get(environment.corsUrl + this.encodeURIComponent(this.COMMUNITY_HOUSEPRICE_DETAIL_URL + buildDetailId));
  }

  public queryMarket591List(buildId: string = '7329'): Observable<any> {
    return this.httpclient.get(environment.corsUrl + this.encodeURIComponent(this.MARKET_591_HOUSEPRICE_URL + 'lists?community_id=' + buildId + '&split_park=1&page=0&page_size=999&_source=0'));
  }

  public queryMarket591Detail(buildDetailId: string = '5895415'): Observable<any> {
    return this.httpclient.get(environment.corsUrl + this.encodeURIComponent(this.MARKET_591_HOUSEPRICE_DETAIL_URL + buildDetailId + '&split_park=1'));
  }

}
