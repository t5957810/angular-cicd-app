
function get_version() {
  major=$(echo "$VERSION" | cut -d. -f1)
  minor=$(echo "$VERSION" | cut -d. -f2)
  patch=$(echo "$VERSION" | cut -d. -f3)
  patch=$((patch + 1))
  if [ "$patch" -eq 100 ]; then
    patch=0
    minor=$((minor + 1))
    if [ "$minor" -eq 100 ]; then
      minor=0
      major=$((major + 1))
    fi
  fi
  NEW_VERSION="$major.$minor.$patch"
  echo "$NEW_VERSION"
}

function update_package_json_version() {
  NEW_VERSION=$(get_version)

  jq --arg version "$NEW_VERSION" '.version = $version' package.json >tmp.json && mv tmp.json package.json
}

function docker_build() {
  NEW_VERSION=$(get_version)

  # Docker build & push to Gitlab Container Registry
  docker build -t ${CI_REGISTRY_IMAGE}:${NEW_VERSION} .
  docker tag ${CI_REGISTRY_IMAGE}:${NEW_VERSION} ${CI_REGISTRY_IMAGE}:${NEW_VERSION}
  echo "$CI_REGISTRY_PASSWORD" | docker login -u "$CI_REGISTRY_USER" --password-stdin $CI_REGISTRY
  docker push ${CI_REGISTRY_IMAGE}:${NEW_VERSION}

  echo "docker push end"

  curl --request PUT --header "PRIVATE-TOKEN: ${CD_ACCESS_TOKEN}" "${GITLAB_URL}api/v4/projects/${CI_PROJECT_ID}/variables/VERSION" --form "value=${NEW_VERSION}"
  curl --request POST --header "PRIVATE-TOKEN: ${CD_ACCESS_TOKEN}" "${GITLAB_URL}api/v4/projects/${CI_PROJECT_ID}/repository/tags?tag_name=${NEW_VERSION}&ref=${CI_COMMIT_REF_NAME}"


}
